[string]$in = "h:\temp\machines.txt"
[string]$out = "h:\temp\out.txt"

clear

write-host "Gathering informaion from........" 
echo " " > $out

try {

		foreach ($server in (Get-Content $in)) {
			write-host "$server" 
			echo "############   $server   ##########" >> $out
			echo "------------ JAVA_HOME -----------" >> $out
			$Jhome = Get-WMIObject -Class Win32_Environment -ComputerName $server -EA Stop | Where-Object {$_.Name -eq "JAVA_HOME"}  
			#echo $Jhome.variablevalue >> $out	
				if (!$Jhome){
				echo "JAVA_HOME is not set as an environment variable on this server" >> $out
				}
				else {
						 if ( Test-Path ($Jhome.variablevalue + "\bin\java.exe")){ 
						 echo $Jhome.variablevalue >> $out
						 $Jpath = $Jhome.variablevalue + "\bin\java.exe"
						 $sb = {Get-Item "$using:Jpath" | fl versioninfo }
						 Invoke-Command -ComputerName $server -ScriptBlock $sb >> $out 
						 }
						 else {
						 echo "Java_Home is set to $Jhome.variablevalue" >> $out
						 echo "Could not find Java.exe in $Jhome.variablevalue" >> $out
						 }
				}
				
			echo "-----------------------------------" >> $out
			echo "         JAVA VERSION INSATLLED    " >> $out
			Get-WmiObject Win32_Product -Computer $server | Where {$_.Name -match "Java"} >> $out
 
		}

	write-host "Extraction Complete"

	}
	catch {
	 $ErrorMessage = $_.Exception.Message
	 $FailedItem = $_.Exception.ItemName
	 write-host "Error: $ErrorMessage"
	 write-host "FailedItem: $FailedItem"
	 
	
	}
	